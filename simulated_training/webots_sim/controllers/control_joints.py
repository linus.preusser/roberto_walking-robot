"""control_joints controller."""

# You may need to import some classes of the controller module. Ex:
#  from controller import Robot, Motor, DistanceSensor
from controller import Robot
from controller import Motor

# create the Robot instance.
robot = Robot()

# get the time step of the current world.
timestep = int(robot.getBasicTimeStep())

# You should insert a getDevice-like function in order to get the
# instance of a device of the robot. Something like:
motors= [[0] * 3 for _ in range(4)]
print(motors)
for i in range(4):
    for j in range(3):
        motors[i][j] = robot.getDevice('joint'+str(i+1)+'_'+str(j+1))
#  motor = robot.getDevice('motorname')
#  ds = robot.getDevice('dsname')
#  ds.enable(timestep)

# Main loop:
# - perform simulation steps until Webots is stopping the controller
while robot.step(timestep) != -1:
    # Read the sensors:
    # Enter here functions to read sensor data, like:
    #  val = ds.getValue()

    # Process sensor data here.

    # Enter here functions to send actuator commands, like:
    #  motor.setPosition(10.0)
    for i in range(4):
        for j in range(3):
            motors[i][j].setPosition(0.5)
    pass

# Enter here exit cleanup code.
