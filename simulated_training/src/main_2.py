import gym, ray
import tensorflow as tf
from ray.rllib.agents import ppo
from ray.tune.registry import register_env

import webots_environment

ray.init()

def create_env(config):
    env =  gym.make('WebotsEnv-v0')
    env._max_episode_steps = 200
    return env


register_env("WebotsEnvironment", create_env)
trainer = ppo.PPOTrainer(env="WebotsEnvironment", config={
    "env_config": {},
    "render_env": False,
    "record_env": False,
    "log_level": '',
    "num_workers": 1,
    #"framework": 'tfe',
})
trainer.import_model("simulated_training/src/saved_model.pb")
for _ in range(10000):
    print(trainer.train())
