#Webots classes
from os import closerange
from controller import Robot
from controller import Motor
from controller import Node
from controller import Supervisor
from controller import Compass

#used for calculating direction
import math

class Simulation:
    def __init__(self):
        self._robot = Supervisor()
        self.reset()
        self._bodyNode = self._robot.getFromDef("ROBERTO")#if null it failed
        self._timestep = int(self._robot.getBasicTimeStep()) #timestep must be an integer

        self._feedback = [0] * 4

        self._motors = [[0] * 3 for _ in range(4)]
        #get motor device for every joint
        for i in range(4):
            for j in range(3):
                self._motors[i][j] = self._robot.getDevice('joint'+str(i+1)+'_'+str(j+1))
        
        #get compass device
        self._compass = self._robot.getDevice('compass')
        self._compass.enable(self._timestep)

    def step(self, action, actions, feedback): #should return all sensory information too
        if self._robot.step(self._timestep) != -1:
            if action is True:
                #print(actions)
                self.updateJoints(actions)

            if feedback is True:
                self._feedback[0] = self.getPosition()[0]
                self._feedback[1] = self.getPosition()[1]
                self._feedback[2] = self.getPosition()[2]
                self._feedback[3] = self.getDirection()

                return self._feedback
            else:
                return None
        else:
            print("Error occured!")
            pass
    
    def reset(self):
        print("simulation resetting")
        self._robot.simulationReset()

    def getPosition(self):
       return self._bodyNode.getPosition()
    
    def getRotation(self):
        return self._bodyNode.getField("rotation").getSFRotation()
    
    def getTimestep(self):
        return self._timestep
    
    def getDirection(self):
        vec = self._compass.getValues()
        angle = math.atan2(vec[0],vec[2])
        return angle
    
    def getTilt(self):
        vec = self.getRotation()
        return 1-abs(vec[1])
    
    def updateJoints(self, pos):
        for i in range(12):
                self._motors[int(i/3)][i%3].setPosition(float(pos[i]))

    def startRecording(self):
        print("starting to record...")

    def stopRecording(self):
        print("recording stopped.")
 