import os
import tensorflow as tf
from tensorflow.keras.preprocessing import image

pb_model_dir = ".tf_exports"
h5_model = "simulated_training/src/h5model"

# Loading the Tensorflow Saved Model (PB)
model = tf.keras.models.load_model(pb_model_dir)
print("loaded model yay")
#print(model.summary())

# Saving the Model in H5 Format
tf.keras.models.save_model(model, h5_model)
print("saved_model")

# Loading the H5 Saved Model
loaded_model_from_h5 = tf.keras.models.load_model(h5_model)
print(loaded_model_from_h5.summary())