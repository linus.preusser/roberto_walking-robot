# Roberto - a robot that learns to walk

## Abstract
some explaining here and there

## Results
### Progress (500, 600, 1M, 3M steps)
 <img src="documentation/gen500.gif" width="200"/> <img src="documentation/gen600.gif" width="200"/> <img src="documentation/gen1M.gif" width="200"/> <img src="documentation/gen3M.gif" width="200"/>
### Data Logging: reward historgram, mean reward
<img src="documentation/reward_histogram.png" width="400"/> <img src="documentation/reward_mean.png" width="400"/>

### Ground contact visualization
<img src="documentation/floor_contact.gif" width="400"/>
